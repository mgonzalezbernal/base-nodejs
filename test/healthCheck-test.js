/* eslint-disable no-undef */
const assert = require('assert');
const request = require('supertest');

const app = require('../setup/server');

describe('Test HealthCheck end points', () => {
  it('Check of API in /health', (done) => {
    request(app)
      .get('/health')
      .end((_err, response) => {
        assert(response.body.message === 'Hi, I am fine thanks!');
        done();
      });
  });

  it('Check Alive of API in /alive', (done) => {
    request(app)
      .get('/alive')
      .end((_err, response) => {
        assert(response.body.api === 'Api Rest Technical Assessment');
        done();
      });
  });
});
