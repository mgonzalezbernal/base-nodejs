const _ = require('lodash');

function isValidToken(token) {
  const { API_TOKEN } = process.env;
  const result = new Promise((resolve, reject) => {
    try {
      const payload = _.isEqual(token, API_TOKEN);
      if (payload) {
        resolve(payload);
      } else {
        reject(new Error({ status: 500, message: 'Invalid Token' }));
      }
    } catch (err) {
      reject(new Error({ status: 500, message: 'Invalid Token' }));
    }
  });
  return result;
}

module.exports = isValidToken;
