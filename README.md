# Product information

* Name: API rest to Tecnical Assessment
* Description: This API rest is a CRUD of users and articles
* Technologies
  * JavaScript
  * NodeJS
  * express
  * MongoDB you need to be installed
  * Dockerfile

## Installation Tools

* You need to be installed mongo in your mac/win/linux
in case you don't have mongo you can download in the next link

click this [Link to Download MongoDB Community](https://docs.mongodb.com/manual/administration/install-community/)

-You need to have install Docker
[Link to Docker](https://docs.docker.com/install/)

* If you don't want use npm
* Install Homebrew

```/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"```

* Install Yarn

```brew install yarn```

## Clone the repository & run project

* 1 - Clone the project
```git clone https://mgonzalezbernal@bitbucket.org/mgonzalezbernal/technical-assessment-workast.git```

* 2 - Go to folder
```cd technical-assessment-workast```

* 3 - Copy .env file
```cp .env.example .env```

* 4 - Install dependencies with Yarn or Npm

### Install with Yarn

```yarn install```

### Install with Npm

```npm install```

### Run project

```make up```

### Down project

```make Down```

### Logs

```make logs```

### Tests

```make test```

### Logs api

```docker logs -f api```

### Endpoint without API_TOKEN HelthCheck

* Alive Api and version GET

```http://localhost:3000/alive```

* health API GET

```http://localhost:3000/health```

## Postman

[Link to see documentations](https://documenter.getpostman.com/view/352215/SzKZrvvR)

## Swagger API docs

* 1- Run Project  ```make up```

* 2- click this [Link to see documentations](http://localhost:3000/api-docs/)
